﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace atbm_ck
{
    public partial class QLBB : Form
    {

        Dictionary<string, int> integersvaiTro = new Dictionary<string, int>();

        public static string int_quyenTV;
        public static string maTV;
        public static string hoTenTV;
        public static string gioiTinhTV;
        public static string queQuanTV;
        public static string namSinhTV;
        public static string quocTichTV;
        public static string dcThuongTruTV;
        public static string dcTamTruTV;
        public static string donViTV;
        public static string chiNhanhTV;
        public static string congTacTV;
        public static string tamNghiTV;
        public static string luongTV;
        public static string quyenTV;

        public QLBB()
        {
            InitializeComponent();
            string sql = "SELECT TV.MA_THANHVIEN, TV.HOTEN, TV.GIOITINH, TV.QUEQUAN, TV.NAMSINH, TV.QUOCTICH, TV.DC_THUONGTRU, TV.DC_TAMTRU, TV.DONVI, TV.CHINHANH, TV.CONGTAC, TV.TAMNGHI, TV.LUONG, TK_Q.QUYEN "
             + " FROM c##QLBB.THANHVIEN TV, c##QLBB.TAIKHOAN_QUYEN TK_Q"
             + " WHERE TK_Q.QUYEN <> 0 AND TK_Q.IDNGUOIDUNG = TV.MA_THANHVIEN";
            DataTable dt = new DataTable();
            dt = Connection.GetDataTable(sql);
            dgvDS_User.DataSource = dt;

            integersvaiTro.Add("Ban tổ chức", 1);
            integersvaiTro.Add("Tổ giám sát", 2);
            integersvaiTro.Add("Tổ theo dõi", 3);
            integersvaiTro.Add("Tổ lập phiếu", 4);
            integersvaiTro.Add("Ứng cử viên", 5);
            integersvaiTro.Add("Thành viên", 6);

            cboVaiTro.Items.Add("Ban tổ chức");
            cboVaiTro.Items.Add("Tổ giám sát");
            cboVaiTro.Items.Add("Tổ theo dõi");
            cboVaiTro.Items.Add("Tổ lập phiếu");
            cboVaiTro.Items.Add("Ứng cử viên");
            cboVaiTro.Items.Add("Thành viên");

        }



        private void btnSuaTV_Click(object sender, EventArgs e)
        {
            var frm = new SuaTV();
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }

        private void btnDangXuat_Click(object sender, EventArgs e)
        {
            var frm = new fDangNhap();
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            var frm = new ThemTV();
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }

        private void btnShowUpdate_Click(object sender, EventArgs e)
        {
            grboUpdateRole.Visible = true;
        }

        private void dgvDS_User_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var cell = dgvDS_User.CurrentCell;
            lblTenUser.Text = dgvDS_User.CurrentRow.Cells["HOTEN"].Value.ToString();
            hoTenTV = lblTenUser.Text;
            gioiTinhTV = dgvDS_User.CurrentRow.Cells["GIOITINH"].Value.ToString();
            queQuanTV = dgvDS_User.CurrentRow.Cells["QUEQUAN"].Value.ToString();
            namSinhTV = dgvDS_User.CurrentRow.Cells["NAMSINH"].Value.ToString();
            quocTichTV = dgvDS_User.CurrentRow.Cells["QUOCTICH"].Value.ToString();
            dcThuongTruTV = dgvDS_User.CurrentRow.Cells["DC_THUONGTRU"].Value.ToString();
            dcTamTruTV = dgvDS_User.CurrentRow.Cells["DC_TAMTRU"].Value.ToString();
            donViTV = dgvDS_User.CurrentRow.Cells["DONVI"].Value.ToString();
            chiNhanhTV = dgvDS_User.CurrentRow.Cells["CHINHANH"].Value.ToString();
            congTacTV = dgvDS_User.CurrentRow.Cells["CONGTAC"].Value.ToString();
            tamNghiTV = dgvDS_User.CurrentRow.Cells["TAMNGHI"].Value.ToString();
            luongTV = dgvDS_User.CurrentRow.Cells["LUONG"].Value.ToString();
            maTV = dgvDS_User.CurrentRow.Cells["MA_THANHVIEN"].Value.ToString();


            int_quyenTV = dgvDS_User.CurrentRow.Cells["QUYEN"].Value.ToString();

            if (int_quyenTV.Equals("1"))
                quyenTV = "Ban tổ chức";
            else if (int_quyenTV.Equals("2"))
                quyenTV = "Tổ giám sát";
            else if (int_quyenTV.Equals("3"))
                quyenTV = "Tổ theo dõi";
            else if (int_quyenTV.Equals("4"))
                quyenTV = "Tổ lập phiếu";
            else if (int_quyenTV.Equals("5"))
                quyenTV = "Ứng cử viên";
            else if (int_quyenTV.Equals("6"))
                quyenTV = "Thành viên";


            lblcurrentrole.Text = quyenTV;
        }

        private void btnLuuUpdate_Click(object sender, EventArgs e)
        {
            if (lblcurrentrole.Text == cboVaiTro.SelectedItem.ToString())
            {
                MessageBox.Show("Không có gì thay đổi!", "Thông báo", MessageBoxButtons.OK);
            }
            else
            {
                if (cboVaiTro.SelectedItem.ToString() == "Ban tổ chức")
                {
                    string sql;
                    sql = "UPDATE C##QLBB.TAIKHOAN_QUYEN SET QUYEN = '1' WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "INSERT INTO C##QLBB.BTC (MA_BTC, HOTEN, NAMSINH, GIOITINH) VALUES ('"  + maTV + "', '" + hoTenTV + "', '" + Int32.Parse(namSinhTV) + "' , '" + gioiTinhTV + "')";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "GRANT C##BTC_BB TO " +"C##" + maTV + "";
                    Connection.ExcuteQuery(sql);

                    sql = "grant create session to " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);

                    MessageBox.Show("Done", "Thông báo", MessageBoxButtons.OK);
                }
                if (cboVaiTro.SelectedItem.ToString() == "Tổ giám sát")
                {
                    string sql;
                    sql = "UPDATE C##QLBB.TAIKHOAN_QUYEN SET QUYEN = '2' WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);
                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "INSERT INTO C##QLBB.GIAMSAT (MA_GIAMSAT, HOTEN, NAMSINH, GIOITINH) VALUES ('" + maTV + "', '" + hoTenTV + "', '" + Int32.Parse(namSinhTV) + "' , '" + gioiTinhTV + "')";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "GRANT C##GIAMSAT_BB TO " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);
                    sql = "grant create session to " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);
                    MessageBox.Show("Done", "Thông báo", MessageBoxButtons.OK);
                }

                if (cboVaiTro.SelectedItem.ToString() == "Tổ theo dõi")
                {
                    string sql;
                    sql = "UPDATE C##QLBB.TAIKHOAN_QUYEN SET QUYEN = '3' WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);
                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "INSERT INTO C##QLBB.THEODOI (MA_TD, HOTEN, NAMSINH, GIOITINH) VALUES ('"  + maTV + "', '" + hoTenTV + "', '" + Int32.Parse(namSinhTV) + "' , '" + gioiTinhTV + "')";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "GRANT c##THEODOI_BB TO " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);
                    sql = "grant create session to " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);
                    MessageBox.Show("Done", "Thông báo", MessageBoxButtons.OK);
                }

                if (cboVaiTro.SelectedItem.ToString() == "Tổ lập phiếu")
                {
                    string sql;
                    sql = "UPDATE C##QLBB.TAIKHOAN_QUYEN SET QUYEN = '4' WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);
                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "INSERT INTO C##QLBB.TO_LAP (MA_TOLAP, HOTEN, NAMSINH, GIOITINH) VALUES ('"+ maTV + "', '" + hoTenTV + "', '" + Int32.Parse(namSinhTV) + "' , '" + gioiTinhTV + "')";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "GRANT c##TOLAP_BB TO " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);
                    sql = "grant create session to " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);
                    MessageBox.Show("Done", "Thông báo", MessageBoxButtons.OK);
                }

                if (cboVaiTro.SelectedItem.ToString() == "Ứng cử viên")
                {
                    string sql;
                    sql = "UPDATE C##QLBB.TAIKHOAN_QUYEN SET QUYEN = '5' WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);
                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "INSERT INTO C##QLBB.UNGCUVIEN (MA_UV, HOTEN, NAMSINH, CHUCVU, KHOA) VALUES ('" + maTV + "', '" + hoTenTV + "', '" + Int32.Parse(namSinhTV) + "' , null, null)";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "GRANT c##THANHVIEN TO " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);
                    sql = "grant create session to " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);
                    MessageBox.Show("Done", "Thông báo", MessageBoxButtons.OK);
                }

                if (cboVaiTro.SelectedItem.ToString() == "Thành viên")
                {
                    string sql;
                    sql = "UPDATE C##QLBB.TAIKHOAN_QUYEN SET QUYEN = '6' WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);
                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "GRANT c##THANHVIEN TO " +"C##"+ maTV + "";
                    Connection.ExcuteQuery(sql);
                    sql = "grant create session to " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);
                    MessageBox.Show("Done", "Thông báo", MessageBoxButtons.OK);
                }




                if (lblcurrentrole.Text == "Ban tổ chức")
                {
                    string sql;
                    sql = "DELETE FROM C##QLBB.BTC WHERE MA_BTC = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);
                }
                if (lblcurrentrole.Text == "Tổ lập phiếu")
                {
                    string sql;
                    sql = "DELETE FROM C##QLBB.TO_LAP WHERE MA_TOLAP = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);
                }
                if (lblcurrentrole.Text == "Tổ theo dõi")
                {
                    string sql;
                    sql = "DELETE FROM C##QLBB.THEODOI WHERE MA_TD = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);
                }
                if (lblcurrentrole.Text == "Tổ giám sát")
                {
                    string sql;
                    sql = "DELETE FROM C##QLBB.GIAMSAT WHERE MA_GIAMSAT = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);
                }
                if (lblcurrentrole.Text == "Ứng cử viên")
                {
                    string sql;
                    sql = "DELETE FROM C##QLBB.UNGCUVIEN WHERE MA_UV = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);
                }





                else if (lblcurrentrole.Text == "Ứng cử viên")
                {
                    string sql;
                    sql = "DELETE FROM C##QLBB.UNGCUVIEN WHERE MA_UV = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);
                }



                if (cboVaiTro.SelectedItem.ToString() == "")
                {
                    MessageBox.Show("Vui lòng chọn vai trò mới!", "Thông báo", MessageBoxButtons.OK);
                }


                string sql2 = "SELECT TV.MA_THANHVIEN, TV.HOTEN, TV.GIOITINH, TV.QUEQUAN, TV.NAMSINH, TV.QUOCTICH, TV.DC_THUONGTRU, TV.DC_TAMTRU, TV.DONVI, TV.CHINHANH, TV.CONGTAC, TV.TAMNGHI, TV.LUONG, TK_Q.QUYEN "
           + " FROM c##QLBB.THANHVIEN TV, c##QLBB.TAIKHOAN_QUYEN TK_Q"
           + " WHERE TK_Q.QUYEN <> 0 AND TK_Q.IDNGUOIDUNG = TV.MA_THANHVIEN";
                DataTable dt = new DataTable();
                dt = Connection.GetDataTable(sql2);
                dgvDS_User.DataSource = dt;

            }
        }

        private void btnXoaTV_Click(object sender, EventArgs e)
        {
            if (maTV == "")
            {
                MessageBox.Show("Vui lòng chọn một user!", "Thông báo", MessageBoxButtons.OK);
            }
            else
            {
                DialogResult dlr = MessageBox.Show("Bạn có chắc muốn xóa user " + hoTenTV + " ra khỏi hệ thống?", "Thông báo", MessageBoxButtons.YesNo);
                if (dlr == DialogResult.Yes)
                {
                    if (int_quyenTV.Equals("1"))
                    {
                        string sqln = "DELETE FROM C##QLBB.BTC WHERE MA_BTC = '" + maTV + "'";
                        Connection.ExcuteQuery(sqln);

                    }
                    else if (int_quyenTV.Equals("2"))
                    {
                        string sqln = "DELETE FROM C##QLBB.GIAMSAT WHERE MA_GIAMSAT = '" + maTV + "'";
                        Connection.ExcuteQuery(sqln);

                    }
                    else if (int_quyenTV.Equals("3"))
                    {
                        string sqln = "DELETE FROM C##QLBB.THEODOI WHERE MA_TD = '" + maTV + "'";
                        Connection.ExcuteQuery(sqln);

                    }

                    else if (int_quyenTV.Equals("4"))
                    {
                        string sqln = "DELETE FROM C##QLBB.TO_LAP WHERE MA_TOLAP = '" + maTV + "'";
                        Connection.ExcuteQuery(sqln);

                    }
                    else if (int_quyenTV.Equals("5"))
                    {
                        string sqln = "DELETE FROM C##QLBB.UNGCUVIEN WHERE MA_UV = '" + maTV + "'";
                        Connection.ExcuteQuery(sqln);

                    }



                    string sql = "DELETE FROM C##QLBB.THANHVIEN WHERE MA_THANHVIEN = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "DELETE FROM C##QLBB.TAIKHOAN_QUYEN WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "DROP USER " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    MessageBox.Show("Đã xóa!", "Thông báo", MessageBoxButtons.OK);
                }
                else
                {
                    return;
                }
            }
        }

        private void btbThuHoi_Click(object sender, EventArgs e)
        {
            DialogResult dlr = MessageBox.Show("User sẽ không còn connect được vào hệ thống nữa. \nBạn có chắc muốn thu hồi quyền của user " + hoTenTV + " ?", "Thông báo", MessageBoxButtons.YesNo);
            if (dlr == DialogResult.Yes)
            {
                if (int_quyenTV.Equals("1"))
                {
                    string sql = "REVOKE C##BTC_BB FROM "+"C##" + maTV + "";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    string sqln = "DELETE FROM C##QLBB.BTC WHERE MA_BTC = '" + maTV + "'";
                    Connection.ExcuteQuery(sqln);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "UPDATE C##QLBB.TAIKHOAN_QUYEN SET QUYEN = '7' WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    MessageBox.Show("Đã thu hồi!", "Thông báo", MessageBoxButtons.OK);
                }
                else if (int_quyenTV.Equals("2"))
                {
                    string sql = "REVOKE C##GIAMSAT_BB FROM " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    string sqln = "DELETE FROM C##QLBB.GIAMSAT WHERE MA_GIAMSAT = '" + maTV + "'";
                    Connection.ExcuteQuery(sqln);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "UPDATE C##QLBB.TAIKHOAN_QUYEN SET QUYEN = '7' WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    MessageBox.Show("Đã thu hồi!", "Thông báo", MessageBoxButtons.OK);
                }

                else if (int_quyenTV.Equals("3"))
                {
                    string sql = "REVOKE C##THEODOI_BB FROM " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    string sqln = "DELETE FROM C##QLBB.THEODOI WHERE MA_TD = '" + maTV + "'";
                    Connection.ExcuteQuery(sqln);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "UPDATE C##QLBB.TAIKHOAN_QUYEN SET QUYEN = '7' WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    MessageBox.Show("Đã thu hồi!", "Thông báo", MessageBoxButtons.OK);
                }
                else if (int_quyenTV.Equals("4"))
                {
                    string sql = "REVOKE C##TOLAP_BB FROM " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    string sqln = "DELETE FROM C##QLBB.TO_LAP WHERE MA_TOLAP = '" + maTV + "'";
                    Connection.ExcuteQuery(sqln);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "UPDATE C##QLBB.TAIKHOAN_QUYEN SET QUYEN = '7' WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    MessageBox.Show("Đã thu hồi!", "Thông báo", MessageBoxButtons.OK);
                }

                else if (int_quyenTV.Equals("5"))
                {
                    string sql = "REVOKE C##THANHVIEN FROM " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    string sqln = "DELETE FROM C##QLBB.UNGCUVIEN WHERE MA_UV = '" + maTV + "'";
                    Connection.ExcuteQuery(sqln);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    sql = "UPDATE C##QLBB.TAIKHOAN_QUYEN SET QUYEN = '7' WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    MessageBox.Show("Đã thu hồi!", "Thông báo", MessageBoxButtons.OK);
                }

                else if (int_quyenTV.Equals("6"))
                {
                    string sql = "REVOKE C##THANHVIEN FROM " + "C##" + maTV + "";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);


                    sql = "UPDATE C##QLBB.TAIKHOAN_QUYEN SET QUYEN = '7' WHERE IDNGUOIDUNG = '" + maTV + "'";
                    Connection.ExcuteQuery(sql);

                    sql = "commit";
                    Connection.ExcuteQuery(sql);

                    MessageBox.Show("Đã thu hồi!", "Thông báo", MessageBoxButtons.OK);
                }

                string sqla = "SELECT TV.MA_THANHVIEN, TV.HOTEN, TV.GIOITINH, TV.QUEQUAN, TV.NAMSINH, TV.QUOCTICH, TV.DC_THUONGTRU, TV.DC_TAMTRU, TV.DONVI, TV.CHINHANH, TV.CONGTAC, TV.TAMNGHI, TV.LUONG, TK_Q.QUYEN "
           + " FROM c##QLBB.THANHVIEN TV, c##QLBB.TAIKHOAN_QUYEN TK_Q"
           + " WHERE TK_Q.QUYEN <> 0 AND TK_Q.IDNGUOIDUNG = TV.MA_THANHVIEN";
                DataTable dt = new DataTable();
                dt = Connection.GetDataTable(sqla);
                dgvDS_User.DataSource = dt;

            }
            else
            {
                return;
            }
        }

        private void btnSuaTV_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show("Chưa hoàn thành!", "Thông báo", MessageBoxButtons.OK);
        }
    }
  
}
    