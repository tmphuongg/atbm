﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace atbm_ck
{
    public partial class ThemTV : Form
    {
        Dictionary<string, int> integersvaiTro = new Dictionary<string, int>();
     
        public ThemTV()
        {
            InitializeComponent();

            cboGioiTinh.Items.Add("Nữ");
            cboGioiTinh.Items.Add("Nam");
            cboGioiTinh.Items.Add("Khác");

            integersvaiTro.Add("Ban tổ chức", 1);
            integersvaiTro.Add("Tổ giám sát", 2);
            integersvaiTro.Add("Tổ theo dõi", 3);
            integersvaiTro.Add("Tổ lập phiếu", 4);
            integersvaiTro.Add("Ứng cử viên", 5);
            integersvaiTro.Add("Thành viên", 6);


            cboVaiTro.Items.Add("Ban tổ chức");
            cboVaiTro.Items.Add("Tổ giám sát");
            cboVaiTro.Items.Add("Ứng cử viên");
            cboVaiTro.Items.Add("Tổ theo dõi");
            cboVaiTro.Items.Add("Tổ lập phiếu");
            cboVaiTro.Items.Add("Thành viên");

        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            string sql = "INSERT INTO C##QLBB.THANHVIEN (MA_THANHVIEN, HOTEN, GIOITINH, QUEQUAN, NAMSINH, QUOCTICH, DC_THUONGTRU, DC_TAMTRU, DONVI, CHINHANH, CONGTAC, TAMNGHI, LUONG)"
                     + " VALUES ('"  + txt_maTV.Text + "', '" + txtHoTen.Text + "', '" + cboGioiTinh.SelectedItem.ToString() + "', '" + txtQueQuan.Text + "', '" + Int32.Parse(txtNamSinh.Text) + "', '" + txtQuocTich.Text + "', '" + txt_dcThuongTru.Text + "', '" + txt_dcTamTru.Text + "', '', '', '', '" + txtTamNghi.Text + "', '" + txtLuong.Text + "')";

            Connection.ExcuteQuery(sql);
            sql = "commit";
            Connection.ExcuteQuery(sql);
            if (cboVaiTro.SelectedItem.ToString() == "Ban tổ chức")
            {
                sql = "INSERT INTO C##QLBB.TAIKHOAN_QUYEN (IDNGUOIDUNG, TENTAIKHOAN, MATKHAU, QUYEN) VALUES ('" + txt_maTV.Text + "', '" + "C##" + txt_maTV.Text + "', '" + txb_pass.Text + "', '1')";
                Connection.ExcuteQuery(sql);

                sql = "commit";
                Connection.ExcuteQuery(sql);

                sql = "INSERT INTO C##QLBB.BTC (MA_BTC, HOTEN, NAMSINH, GIOITINH) VALUES ('" + txt_maTV.Text + "', '" + txtHoTen.Text + "', '" + Int32.Parse(txtNamSinh.Text) + "' , '" + cboGioiTinh.SelectedItem.ToString() + "')";
                Connection.ExcuteQuery(sql);

                sql = "commit";
                Connection.ExcuteQuery(sql);

                sql = "CREATE user " + "c##" + txt_maTV.Text + " IDENTIFIED BY " + txb_pass.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "grant create session to " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "GRANT c##THANHVIEN TO " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "GRANT c##BTC_BB TO " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

                
            }
            if (cboVaiTro.SelectedItem.ToString() == "Tổ giám sát")
            {
                sql = "INSERT INTO C##QLBB.TAIKHOAN_QUYEN (IDNGUOIDUNG, TENTAIKHOAN, MATKHAU, QUYEN) VALUES ('" + txt_maTV.Text + "', '" + "C##" + txt_maTV.Text + "', '" + txb_pass.Text + "', '2')";
                Connection.ExcuteQuery(sql);

                sql = "commit";
                Connection.ExcuteQuery(sql);

                sql = "INSERT INTO C##QLBB.GIAMSAT (MA_GIAMSAT, HOTEN, NAMSINH, GIOITINH) VALUES ('"  + txt_maTV.Text + "', '" + txtHoTen.Text + "', '" + Int32.Parse(txtNamSinh.Text) + "' , '" + cboGioiTinh.SelectedItem.ToString() + "')";
                Connection.ExcuteQuery(sql);

                sql = "commit";
                Connection.ExcuteQuery(sql);

                sql = "CREATE user " + "c##" + txt_maTV.Text + " IDENTIFIED BY " + txb_pass.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "grant create session to " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "GRANT c##THANHVIEN TO " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "GRANT c##GIAMSAT_BB TO " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);
            }


            if (cboVaiTro.SelectedItem.ToString() == "Tổ theo dõi")
            {
                sql = "INSERT INTO C##QLBB.TAIKHOAN_QUYEN (IDNGUOIDUNG, TENTAIKHOAN, MATKHAU, QUYEN) VALUES ('"  + txt_maTV.Text + "', '" + "C##" + txt_maTV.Text + "', '" + txb_pass.Text + "', '3')";
                Connection.ExcuteQuery(sql);

                sql = "commit";
                Connection.ExcuteQuery(sql);

                sql = "INSERT INTO C##QLBB.THEODOI (MA_TD, HOTEN, NAMSINH, GIOITINH) VALUES ('" + txt_maTV.Text + "', '" + txtHoTen.Text + "', '" + Int32.Parse(txtNamSinh.Text) + "' , '" + cboGioiTinh.SelectedItem.ToString() + "')";
                Connection.ExcuteQuery(sql);

                sql = "commit";
                Connection.ExcuteQuery(sql);

                sql = "CREATE user " + "c##" + txt_maTV.Text + " IDENTIFIED BY " + txb_pass.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "grant create session to " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "GRANT c##THANHVIEN TO " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "GRANT c##THEODOI_BB TO " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);
            }

            if (cboVaiTro.SelectedItem.ToString() == "Tổ lập phiếu")
            {
                sql = "INSERT INTO C##QLBB.TAIKHOAN_QUYEN (IDNGUOIDUNG, TENTAIKHOAN, MATKHAU, QUYEN) VALUES ('"  + txt_maTV.Text + "', '" + "C##" + txt_maTV.Text + "', '" + txb_pass.Text + "', '4')";
                Connection.ExcuteQuery(sql);

                sql = "commit";
                Connection.ExcuteQuery(sql);

                sql = "INSERT INTO C##QLBB.TO_LAP (MA_TOLAP, HOTEN, NAMSINH, GIOITINH) VALUES ('" + txt_maTV.Text + "', '" + txtHoTen.Text + "', '" + Int32.Parse(txtNamSinh.Text) + "' , '" + cboGioiTinh.SelectedItem.ToString() + "')";
                Connection.ExcuteQuery(sql);

                sql = "commit";
                Connection.ExcuteQuery(sql);

                sql = "CREATE user " + "c##" + txt_maTV.Text + " IDENTIFIED BY " + txb_pass.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "grant create session to " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "GRANT c##THANHVIEN TO " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "GRANT c##TOLAP_BB TO " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);
            }


            if (cboVaiTro.SelectedItem.ToString() == "Ứng cử viên")
            {
                sql = "INSERT INTO C##QLBB.TAIKHOAN_QUYEN (IDNGUOIDUNG, TENTAIKHOAN, MATKHAU, QUYEN) VALUES ('" + txt_maTV.Text + "', '" + "C##" + txt_maTV.Text + "', '" + txb_pass.Text + "', '5')";
                Connection.ExcuteQuery(sql);

                sql = "commit";
                Connection.ExcuteQuery(sql);

                sql = "INSERT INTO C##QLBB.UNGCUVIEN (MA_UV, HOTEN, NAMSINH, CHUCVU, KHOA) VALUES ('" + txt_maTV.Text + "', '" + txtHoTen.Text + "', '" + Int32.Parse(txtNamSinh.Text) + "' ,null, null)";
                Connection.ExcuteQuery(sql);

                sql = "commit";
                Connection.ExcuteQuery(sql);

                sql = "CREATE user " + "c##" + txt_maTV.Text + " IDENTIFIED BY " + txb_pass.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "grant create session to " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "GRANT c##UNGCUVIEN TO " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

            }

            if (cboVaiTro.SelectedItem.ToString() == "Thành viên")
            {
                sql = "INSERT INTO C##QLBB.TAIKHOAN_QUYEN (IDNGUOIDUNG, TENTAIKHOAN, MATKHAU, QUYEN) VALUES ('" + txt_maTV.Text + "', '" + "C##" + txt_maTV.Text + "', '" + txb_pass.Text + "', '6')";
                Connection.ExcuteQuery(sql);

                sql = "commit";
                Connection.ExcuteQuery(sql);

                sql = "CREATE user " + "c##" + txt_maTV.Text + " IDENTIFIED BY " + txb_pass.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "grant create session to " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

                sql = "GRANT c##THANHVIEN TO " + "c##" + txt_maTV.Text + "";
                Connection.ExcuteQuery(sql);

            }

            sql = "commit";
            Connection.ExcuteQuery(sql);

            sql = "UPDATE THANHVIEN SET LUONG = CODING_VC2.ENCODE(CONCAT(MA_THANHVIEN, LUONG)) where MA_THANHVIEN = '"+txt_maTV.Text+"'";
            Connection.ExcuteQuery(sql);



            DialogResult dlr = MessageBox.Show("Tạo thành công!", "Thông báo", MessageBoxButtons.OK);
            if (dlr == DialogResult.OK)
            {
                var frm = new QLBB();
                this.Hide();
                frm.ShowDialog();
                this.Close();
            }


        }

        private void cboVaiTro_SelectedIndexChanged(object sender, EventArgs e)
        {
         
                txt_maTV.Text = "TV";
        
        }

     
    }
}
