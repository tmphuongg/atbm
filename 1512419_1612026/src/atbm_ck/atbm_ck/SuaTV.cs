﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace atbm_ck
{
    public partial class SuaTV : Form
    {
        public SuaTV()
        {
            InitializeComponent();

            txtHoTen.Text = QLBB.hoTenTV;
            txtGioiTinh.Text = QLBB.gioiTinhTV;
            txtNamSinh.Text = QLBB.namSinhTV;
            txtQuocTich.Text = QLBB.quocTichTV;
            txtQueQuan.Text = QLBB.queQuanTV;
            txt_dcThuongTru.Text = QLBB.dcThuongTruTV;
            txt_dcTamTru.Text = QLBB.dcTamTruTV;
            txtDonVi.Text = QLBB.donViTV;
            txtCongTac.Text = QLBB.congTacTV;
            txtLuong.Text = QLBB.luongTV;
            txtChinhanh.Text = QLBB.chiNhanhTV;
            txtTamNghi.Text = QLBB.tamNghiTV;
            txtQuyen.Text = QLBB.quyenTV;
              
        }
    }
}
