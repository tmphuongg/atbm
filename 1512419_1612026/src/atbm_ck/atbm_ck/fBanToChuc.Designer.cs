﻿namespace atbm_ck
{
    partial class fBanToChuc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvdsUCV = new System.Windows.Forms.DataGridView();
            this.btnXoaDS = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnDangXuat = new System.Windows.Forms.Button();
            this.SuaUCV = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txb_xoaungcuvien = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvDS_TL = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.gdvTheoDoi = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgvDS_GS = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dgvDS_NDB = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdsUCV)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDS_TL)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdvTheoDoi)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDS_GS)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDS_NDB)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvdsUCV
            // 
            this.dgvdsUCV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvdsUCV.Location = new System.Drawing.Point(3, 3);
            this.dgvdsUCV.Name = "dgvdsUCV";
            this.dgvdsUCV.Size = new System.Drawing.Size(1078, 314);
            this.dgvdsUCV.TabIndex = 0;
            this.dgvdsUCV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvdsUCV_CellContentDoubleClick);
            this.dgvdsUCV.SelectionChanged += new System.EventHandler(this.dgvdsUCV_CurrentCellChanged);
            // 
            // btnXoaDS
            // 
            this.btnXoaDS.Location = new System.Drawing.Point(634, 369);
            this.btnXoaDS.Name = "btnXoaDS";
            this.btnXoaDS.Size = new System.Drawing.Size(87, 27);
            this.btnXoaDS.TabIndex = 0;
            this.btnXoaDS.Text = "Xóa UCV";
            this.btnXoaDS.UseVisualStyleBackColor = true;
            this.btnXoaDS.Click += new System.EventHandler(this.btnXoaDS_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1095, 442);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnDangXuat);
            this.tabPage1.Controls.Add(this.SuaUCV);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txb_xoaungcuvien);
            this.tabPage1.Controls.Add(this.dgvdsUCV);
            this.tabPage1.Controls.Add(this.btnXoaDS);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1087, 414);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ứng cử viên";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnDangXuat
            // 
            this.btnDangXuat.Location = new System.Drawing.Point(6, 369);
            this.btnDangXuat.Name = "btnDangXuat";
            this.btnDangXuat.Size = new System.Drawing.Size(87, 27);
            this.btnDangXuat.TabIndex = 8;
            this.btnDangXuat.Text = "Đăng xuất";
            this.btnDangXuat.UseVisualStyleBackColor = true;
            this.btnDangXuat.Visible = false;
            this.btnDangXuat.Click += new System.EventHandler(this.btnDangXuat_Click);
            // 
            // SuaUCV
            // 
            this.SuaUCV.Location = new System.Drawing.Point(750, 369);
            this.SuaUCV.Name = "SuaUCV";
            this.SuaUCV.Size = new System.Drawing.Size(115, 27);
            this.SuaUCV.TabIndex = 7;
            this.SuaUCV.Text = "Sửa thông tin UCV";
            this.SuaUCV.UseVisualStyleBackColor = true;
            this.SuaUCV.Click += new System.EventHandler(this.SuaUCV_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(543, 344);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Đang chọn";
            // 
            // txb_xoaungcuvien
            // 
            this.txb_xoaungcuvien.Location = new System.Drawing.Point(634, 337);
            this.txb_xoaungcuvien.Name = "txb_xoaungcuvien";
            this.txb_xoaungcuvien.Size = new System.Drawing.Size(231, 21);
            this.txb_xoaungcuvien.TabIndex = 5;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvDS_TL);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1087, 414);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tổ lập DS Người đi bầu";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvDS_TL
            // 
            this.dgvDS_TL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDS_TL.Location = new System.Drawing.Point(0, 3);
            this.dgvDS_TL.Name = "dgvDS_TL";
            this.dgvDS_TL.Size = new System.Drawing.Size(1084, 357);
            this.dgvDS_TL.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.gdvTheoDoi);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1087, 414);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Tổ theo dõi kết quả";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // gdvTheoDoi
            // 
            this.gdvTheoDoi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdvTheoDoi.Location = new System.Drawing.Point(3, 3);
            this.gdvTheoDoi.Name = "gdvTheoDoi";
            this.gdvTheoDoi.Size = new System.Drawing.Size(1081, 357);
            this.gdvTheoDoi.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgvDS_GS);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1087, 414);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Tổ giám sát";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dgvDS_GS
            // 
            this.dgvDS_GS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDS_GS.Location = new System.Drawing.Point(3, 3);
            this.dgvDS_GS.Name = "dgvDS_GS";
            this.dgvDS_GS.Size = new System.Drawing.Size(1081, 357);
            this.dgvDS_GS.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dgvDS_NDB);
            this.tabPage5.Location = new System.Drawing.Point(4, 24);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1087, 414);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Danh sách người đi bầu";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dgvDS_NDB
            // 
            this.dgvDS_NDB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDS_NDB.Location = new System.Drawing.Point(3, 3);
            this.dgvDS_NDB.Name = "dgvDS_NDB";
            this.dgvDS_NDB.Size = new System.Drawing.Size(1081, 357);
            this.dgvDS_NDB.TabIndex = 2;
            // 
            // fBanToChuc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1097, 447);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "fBanToChuc";
            this.Text = "Ban tổ chức";
            ((System.ComponentModel.ISupportInitialize)(this.dgvdsUCV)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDS_TL)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gdvTheoDoi)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDS_GS)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDS_NDB)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvdsUCV;
        private System.Windows.Forms.Button btnXoaDS;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dgvDS_TL;
        private System.Windows.Forms.DataGridView gdvTheoDoi;
        private System.Windows.Forms.DataGridView dgvDS_GS;
        private System.Windows.Forms.TextBox txb_xoaungcuvien;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SuaUCV;
        private System.Windows.Forms.Button btnDangXuat;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dgvDS_NDB;
    }
}