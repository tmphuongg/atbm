﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace atbm_ck
{
    public partial class fBanToChuc : Form
    {
        public static string tenUCV;
        public static string namSinhUCV;
        public static string chucvuUCV;
        public static string khoaUCV;
        public static string maUCV;
        public fBanToChuc()
        {
            InitializeComponent();
            string sql = "SELECT * FROM c##QLBB.UNGCUVIEN";
            DataTable dt = new DataTable();
            dt = Connection.GetDataTable(sql);
            dgvdsUCV.DataSource = dt;


            string sql1 = "SELECT * FROM c##QLBB.THEODOI";
            DataTable dt1 = new DataTable();
            dt1 = Connection.GetDataTable(sql1);
            gdvTheoDoi.DataSource = dt1;


            string sql2 = "SELECT * FROM c##QLBB.GIAMSAT";
            DataTable dt2 = new DataTable();
            dt2 = Connection.GetDataTable(sql2);
            dgvDS_GS.DataSource = dt2;

            string sql3 = "SELECT * FROM c##QLBB.THANHVIEN";
            DataTable dt3 = new DataTable();
            dt3 = Connection.GetDataTable(sql3);
            dgvDS_NDB.DataSource = dt3;

            string sql4 = "SELECT * FROM c##QLBB.TO_LAP";
            DataTable dt4 = new DataTable();
            dt4= Connection.GetDataTable(sql4);
            dgvDS_TL.DataSource = dt4;


        }


        private void dgvdsUCV_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var cell = dgvdsUCV.CurrentCell;
            txb_xoaungcuvien.Text = dgvdsUCV.CurrentRow.Cells["HOTEN"].Value.ToString();
            tenUCV = txb_xoaungcuvien.Text;
            namSinhUCV = dgvdsUCV.CurrentRow.Cells["NAMSINH"].Value.ToString();
            chucvuUCV = dgvdsUCV.CurrentRow.Cells["CHUCVU"].Value.ToString();
            khoaUCV = dgvdsUCV.CurrentRow.Cells["KHOA"].Value.ToString();
            maUCV = dgvdsUCV.CurrentRow.Cells["MA_UV"].Value.ToString();
        }



        private void btnXoaDS_Click(object sender, EventArgs e)
        {
            //if (txb_xoaungcuvien.Text == "")
            {
                MessageBox.Show("...........!", "Thông báo");
            }
            //else
            //{
            //    DialogResult dlr = MessageBox.Show("Bạn có chắc chắn muốn xóa UCV " + tenUCV + " ?", "Thông báo", MessageBoxButtons.YesNo);
            //    if (dlr == DialogResult.Yes)
            //    {
            //        string sql = "DELETE FROM c##QLBB.UNGCUVIEN WHERE MA_UV = '" + maUCV + "'";
            //        Connection.ExcuteQuery(sql);
            //        sql = "commit";
    
            //        Connection.ExcuteQuery(sql);
            //        MessageBox.Show("Đã xóa!", "Thông báo", MessageBoxButtons.OK);
            //        string sql1 = "SELECT * FROM C##QLBB.UNGCUVIEN ";
               
            //        DataTable dt = new DataTable();
            //        dt = Connection.GetDataTable(sql1);
            //        dgvdsUCV.DataSource = dt;
            //        txb_xoaungcuvien.Text = "";
            //    }
            //    else
            //    {
            //        return;
            //    }
            //}
        }

        private void dgvdsUCV_CurrentCellChanged(object sender, EventArgs e)
        {
         
        }

        private void SuaUCV_Click(object sender, EventArgs e)
        {
            var frm = new SuaTT_UCV();
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }

        private void btnDangXuat_Click(object sender, EventArgs e)
        {
            var frm = new fDangNhap();
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }
    }
}
