﻿namespace atbm_ck
{
    partial class ToLap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvDS_NDB = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnThemNDB = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDS_NDB)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvDS_NDB
            // 
            this.dgvDS_NDB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDS_NDB.Location = new System.Drawing.Point(3, 35);
            this.dgvDS_NDB.Name = "dgvDS_NDB";
            this.dgvDS_NDB.Size = new System.Drawing.Size(1093, 232);
            this.dgvDS_NDB.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(2, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1110, 421);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnThemNDB);
            this.tabPage1.Controls.Add(this.dgvDS_NDB);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1102, 395);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Danh sách người đi bầu";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnThemNDB
            // 
            this.btnThemNDB.Location = new System.Drawing.Point(6, 6);
            this.btnThemNDB.Name = "btnThemNDB";
            this.btnThemNDB.Size = new System.Drawing.Size(75, 23);
            this.btnThemNDB.TabIndex = 1;
            this.btnThemNDB.Text = "Thêm";
            this.btnThemNDB.UseVisualStyleBackColor = true;
            // 
            // ToLap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1114, 426);
            this.Controls.Add(this.tabControl1);
            this.Name = "ToLap";
            this.Text = "Tổ lập danh sách người đi bầu";
            ((System.ComponentModel.ISupportInitialize)(this.dgvDS_NDB)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDS_NDB;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnThemNDB;
    }
}