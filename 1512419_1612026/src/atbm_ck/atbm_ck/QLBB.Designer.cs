﻿namespace atbm_ck
{
    partial class QLBB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnShowUpdate = new System.Windows.Forms.Button();
            this.lblTenUser = new System.Windows.Forms.Label();
            this.grboUpdateRole = new System.Windows.Forms.GroupBox();
            this.btbThuHoi = new System.Windows.Forms.Button();
            this.btnLuuUpdate = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.lblcurrentrole = new System.Windows.Forms.Label();
            this.lblVaiTroHienTai = new System.Windows.Forms.Label();
            this.cboVaiTro = new System.Windows.Forms.ComboBox();
            this.btnThem = new System.Windows.Forms.Button();
            this.btnDangXuat = new System.Windows.Forms.Button();
            this.btnSuaTV = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnXoaTV = new System.Windows.Forms.Button();
            this.dgvDS_User = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.grboUpdateRole.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDS_User)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(2, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1093, 463);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnShowUpdate);
            this.tabPage1.Controls.Add(this.lblTenUser);
            this.tabPage1.Controls.Add(this.grboUpdateRole);
            this.tabPage1.Controls.Add(this.btnThem);
            this.tabPage1.Controls.Add(this.btnDangXuat);
            this.tabPage1.Controls.Add(this.btnSuaTV);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.btnXoaTV);
            this.tabPage1.Controls.Add(this.dgvDS_User);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1085, 435);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "User";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnShowUpdate
            // 
            this.btnShowUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowUpdate.Location = new System.Drawing.Point(209, 350);
            this.btnShowUpdate.Name = "btnShowUpdate";
            this.btnShowUpdate.Size = new System.Drawing.Size(141, 27);
            this.btnShowUpdate.TabIndex = 38;
            this.btnShowUpdate.Text = "Cập nhật vai trò";
            this.btnShowUpdate.UseVisualStyleBackColor = true;
            this.btnShowUpdate.Click += new System.EventHandler(this.btnShowUpdate_Click);
            // 
            // lblTenUser
            // 
            this.lblTenUser.AutoSize = true;
            this.lblTenUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenUser.Location = new System.Drawing.Point(109, 323);
            this.lblTenUser.Name = "lblTenUser";
            this.lblTenUser.Size = new System.Drawing.Size(31, 15);
            this.lblTenUser.TabIndex = 37;
            this.lblTenUser.Text = "null";
            // 
            // grboUpdateRole
            // 
            this.grboUpdateRole.Controls.Add(this.btbThuHoi);
            this.grboUpdateRole.Controls.Add(this.btnLuuUpdate);
            this.grboUpdateRole.Controls.Add(this.label4);
            this.grboUpdateRole.Controls.Add(this.lblcurrentrole);
            this.grboUpdateRole.Controls.Add(this.lblVaiTroHienTai);
            this.grboUpdateRole.Controls.Add(this.cboVaiTro);
            this.grboUpdateRole.Location = new System.Drawing.Point(375, 305);
            this.grboUpdateRole.Name = "grboUpdateRole";
            this.grboUpdateRole.Size = new System.Drawing.Size(244, 126);
            this.grboUpdateRole.TabIndex = 20;
            this.grboUpdateRole.TabStop = false;
            this.grboUpdateRole.Text = "Cập nhật vai trò";
            this.grboUpdateRole.Visible = false;
            // 
            // btbThuHoi
            // 
            this.btbThuHoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btbThuHoi.Location = new System.Drawing.Point(9, 90);
            this.btbThuHoi.Name = "btbThuHoi";
            this.btbThuHoi.Size = new System.Drawing.Size(82, 27);
            this.btbThuHoi.TabIndex = 40;
            this.btbThuHoi.Text = "Thu hồi";
            this.btbThuHoi.UseVisualStyleBackColor = true;
            this.btbThuHoi.Click += new System.EventHandler(this.btbThuHoi_Click);
            // 
            // btnLuuUpdate
            // 
            this.btnLuuUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuuUpdate.Location = new System.Drawing.Point(151, 90);
            this.btnLuuUpdate.Name = "btnLuuUpdate";
            this.btnLuuUpdate.Size = new System.Drawing.Size(82, 27);
            this.btnLuuUpdate.TabIndex = 39;
            this.btnLuuUpdate.Text = "Lưu";
            this.btnLuuUpdate.UseVisualStyleBackColor = true;
            this.btnLuuUpdate.Click += new System.EventHandler(this.btnLuuUpdate_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 15);
            this.label4.TabIndex = 36;
            this.label4.Text = "Vai trò mới";
            // 
            // lblcurrentrole
            // 
            this.lblcurrentrole.AutoSize = true;
            this.lblcurrentrole.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcurrentrole.Location = new System.Drawing.Point(108, 27);
            this.lblcurrentrole.Name = "lblcurrentrole";
            this.lblcurrentrole.Size = new System.Drawing.Size(108, 15);
            this.lblcurrentrole.TabIndex = 35;
            this.lblcurrentrole.Text = "Vai trò hiện tại: ";
            // 
            // lblVaiTroHienTai
            // 
            this.lblVaiTroHienTai.AutoSize = true;
            this.lblVaiTroHienTai.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVaiTroHienTai.Location = new System.Drawing.Point(7, 27);
            this.lblVaiTroHienTai.Name = "lblVaiTroHienTai";
            this.lblVaiTroHienTai.Size = new System.Drawing.Size(90, 15);
            this.lblVaiTroHienTai.TabIndex = 21;
            this.lblVaiTroHienTai.Text = "Vai trò hiện tại: ";
            // 
            // cboVaiTro
            // 
            this.cboVaiTro.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboVaiTro.FormattingEnabled = true;
            this.cboVaiTro.Location = new System.Drawing.Point(108, 54);
            this.cboVaiTro.Name = "cboVaiTro";
            this.cboVaiTro.Size = new System.Drawing.Size(125, 23);
            this.cboVaiTro.TabIndex = 34;
            // 
            // btnThem
            // 
            this.btnThem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Location = new System.Drawing.Point(7, 7);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(82, 27);
            this.btnThem.TabIndex = 19;
            this.btnThem.Text = "Thêm TV";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnDangXuat
            // 
            this.btnDangXuat.Location = new System.Drawing.Point(7, 528);
            this.btnDangXuat.Name = "btnDangXuat";
            this.btnDangXuat.Size = new System.Drawing.Size(82, 27);
            this.btnDangXuat.TabIndex = 18;
            this.btnDangXuat.Text = "Đăng xuất";
            this.btnDangXuat.UseVisualStyleBackColor = true;
            // 
            // btnSuaTV
            // 
            this.btnSuaTV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuaTV.Location = new System.Drawing.Point(93, 350);
            this.btnSuaTV.Name = "btnSuaTV";
            this.btnSuaTV.Size = new System.Drawing.Size(110, 27);
            this.btnSuaTV.TabIndex = 17;
            this.btnSuaTV.Text = "Sửa thông tin";
            this.btnSuaTV.UseVisualStyleBackColor = true;
            this.btnSuaTV.Click += new System.EventHandler(this.btnSuaTV_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 323);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 15);
            this.label1.TabIndex = 16;
            this.label1.Text = "Đang chọn:";
            // 
            // btnXoaTV
            // 
            this.btnXoaTV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaTV.Location = new System.Drawing.Point(7, 351);
            this.btnXoaTV.Name = "btnXoaTV";
            this.btnXoaTV.Size = new System.Drawing.Size(54, 27);
            this.btnXoaTV.TabIndex = 14;
            this.btnXoaTV.Text = "Xóa TV";
            this.btnXoaTV.UseVisualStyleBackColor = true;
            this.btnXoaTV.Click += new System.EventHandler(this.btnXoaTV_Click);
            // 
            // dgvDS_User
            // 
            this.dgvDS_User.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDS_User.Location = new System.Drawing.Point(4, 39);
            this.dgvDS_User.Name = "dgvDS_User";
            this.dgvDS_User.Size = new System.Drawing.Size(1078, 260);
            this.dgvDS_User.TabIndex = 13;
            this.dgvDS_User.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDS_User_CellContentClick);
            this.dgvDS_User.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDS_User_CellContentClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1085, 435);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "User chưa có role";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // QLBB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1097, 470);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "QLBB";
            this.Text = "Admin";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.grboUpdateRole.ResumeLayout(false);
            this.grboUpdateRole.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDS_User)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnDangXuat;
        private System.Windows.Forms.Button btnSuaTV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnXoaTV;
        private System.Windows.Forms.DataGridView dgvDS_User;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.GroupBox grboUpdateRole;
        private System.Windows.Forms.ComboBox cboVaiTro;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblcurrentrole;
        private System.Windows.Forms.Label lblVaiTroHienTai;
        private System.Windows.Forms.Label lblTenUser;
        private System.Windows.Forms.Button btnShowUpdate;
        private System.Windows.Forms.Button btnLuuUpdate;
        private System.Windows.Forms.Button btbThuHoi;
    }
}