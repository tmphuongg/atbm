﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace atbm_ck
{
    public partial class fDangNhap : Form
    {
        public static string code;
        public fDangNhap()
        {
            InitializeComponent();
        }

        private void btn_dangnhap_Click(object sender, EventArgs e)
        {
            code = txb_tdn.Text;
            OracleConnection conn = Connection.GetDBConnection("localhost", 1521, "atbm", "C##"+txb_tdn.Text, txb_mk.Text);
            try
            {
                conn.Open();
                DialogResult dlr = MessageBox.Show("Đang đăng nhập...", "Thông báo", MessageBoxButtons.YesNo);
                if (dlr == DialogResult.Yes)
                {
                    string sql = "SELECT QUYEN FROM c##QLBB.TAIKHOAN_QUYEN WHERE TENTAIKHOAN = '" + "C##" + txb_tdn.Text + "' AND MATKHAU = '" + txb_mk.Text + "'";
                    DataTable dt = Connection.GetDataTable(sql);

                    if (dt.Rows[0][0].ToString().Equals("0"))
                    {
                        var frm = new QLBB();
                        this.Hide();
                        frm.ShowDialog();
                        this.Close();
                    }

                    else if (dt.Rows[0][0].ToString().Equals("1"))
                    {
                        var frm = new fBanToChuc();
                        this.Hide();
                        frm.ShowDialog();
                        this.Close();
                    }


                    else if (dt.Rows[0][0].ToString().Equals("2"))
                    {
                        var frm = new fGiamSat();
                        this.Hide();
                        frm.ShowDialog();
                        this.Close();
                    }

                    else if (dt.Rows[0][0].ToString().Equals("3"))
                    {
                        MessageBox.Show("Theo dõi", "Thông báo");
                    }
                    else if (dt.Rows[0][0].ToString().Equals("4"))
                    {
                        // MessageBox.Show("Tổ lập", "Thông báo");
                        var frm = new ToLap();
                        this.Hide();
                        frm.ShowDialog();
                        this.Close();
                    }
                    else if (dt.Rows[0][0].ToString().Equals("5"))
                    {
                        MessageBox.Show("Ứng cử viên", "Thông báo");
                    }
                    else if (dt.Rows[0][0].ToString().Equals("6"))
                    {
                        MessageBox.Show("Thành viên", "Thông báo");
                    }

                    else if (dt.Rows[0][0].ToString().Equals("7"))
                    {
                        MessageBox.Show("Bạn không có quyền đăng nhập vào hệ thống! \n Liên hệ QTV để khắc phục.", "Thông báo");
                    }
 

                }
                else return;
                }
            catch (Exception ex)
            {
                MessageBox.Show("Đăng nhập thất bại", "Thông báo");
                return;
            }
        }
    }
}
