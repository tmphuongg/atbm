﻿namespace atbm_ck
{
    partial class SuaTT_UCV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtHoTenUCV = new System.Windows.Forms.TextBox();
            this.txtNamSinhUCV = new System.Windows.Forms.TextBox();
            this.txtChucVuUCV = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtKhoaUCV = new System.Windows.Forms.TextBox();
            this.btxLuuUCV = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtHoTenUCV
            // 
            this.txtHoTenUCV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoTenUCV.Location = new System.Drawing.Point(79, 12);
            this.txtHoTenUCV.Name = "txtHoTenUCV";
            this.txtHoTenUCV.Size = new System.Drawing.Size(275, 21);
            this.txtHoTenUCV.TabIndex = 0;
            // 
            // txtNamSinhUCV
            // 
            this.txtNamSinhUCV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamSinhUCV.Location = new System.Drawing.Point(79, 39);
            this.txtNamSinhUCV.Name = "txtNamSinhUCV";
            this.txtNamSinhUCV.Size = new System.Drawing.Size(116, 21);
            this.txtNamSinhUCV.TabIndex = 1;
            // 
            // txtChucVuUCV
            // 
            this.txtChucVuUCV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChucVuUCV.Location = new System.Drawing.Point(79, 66);
            this.txtChucVuUCV.Name = "txtChucVuUCV";
            this.txtChucVuUCV.Size = new System.Drawing.Size(275, 21);
            this.txtChucVuUCV.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Năm sinh";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Họ tên";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Chức vụ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Khoa";
            // 
            // txtKhoaUCV
            // 
            this.txtKhoaUCV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKhoaUCV.Location = new System.Drawing.Point(79, 93);
            this.txtKhoaUCV.Name = "txtKhoaUCV";
            this.txtKhoaUCV.Size = new System.Drawing.Size(116, 21);
            this.txtKhoaUCV.TabIndex = 6;
            // 
            // btxLuuUCV
            // 
            this.btxLuuUCV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btxLuuUCV.Location = new System.Drawing.Point(267, 117);
            this.btxLuuUCV.Name = "btxLuuUCV";
            this.btxLuuUCV.Size = new System.Drawing.Size(87, 27);
            this.btxLuuUCV.TabIndex = 8;
            this.btxLuuUCV.Text = "Lưu";
            this.btxLuuUCV.UseVisualStyleBackColor = true;
            // 
            // SuaTT_UCV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 157);
            this.Controls.Add(this.btxLuuUCV);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtKhoaUCV);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtChucVuUCV);
            this.Controls.Add(this.txtNamSinhUCV);
            this.Controls.Add(this.txtHoTenUCV);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "SuaTT_UCV";
            this.Text = "SuaTT_UCV";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtHoTenUCV;
        private System.Windows.Forms.TextBox txtNamSinhUCV;
        private System.Windows.Forms.TextBox txtChucVuUCV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtKhoaUCV;
        private System.Windows.Forms.Button btxLuuUCV;
    }
}