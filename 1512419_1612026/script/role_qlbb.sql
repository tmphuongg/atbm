--connect as sys
CREATE USER C##QLBB IDENTIFIED BY phuongtm;
GRANT CONNECT  TO C##QLBB WITH GRANT OPTION;
GRANT CONNECT, RESOURCE TO C##QLBB;
GRANT CREATE TABLE TO C##QLBB;
GRANT UNLIMITED TABLESPACE TO C##QLBB;
GRANT CREATE ROLE  TO C##QLBB
GRANT CREATE VIEW TO C##QLBB;
--GRANT DBA TO C##QLBB;
GRANT SELECT, INSERT, UPDATE, DELETE ON schema.QLBB TO C##QLBB;
GRANT CREATE USER TO C##QLBB CONTAINER=ALL;
GRANT DROP USER TO C##QLBB CONTAINER=ALL;
-----------------------------

--connect as C##QLBB
CREATE user C##TV1 IDENTIFIED BY tv1;
CREATE user C##TV2 IDENTIFIED BY tv2;
CREATE user C##TV3 IDENTIFIED BY tv3;
CREATE user C##TV4 IDENTIFIED BY tv4;
CREATE user C##TV5 IDENTIFIED BY tv5;
CREATE user C##TV6 IDENTIFIED BY tv6;
CREATE user C##TV7 IDENTIFIED BY tv7;
CREATE user C##TV8 IDENTIFIED BY tv8;
CREATE user C##TV9 IDENTIFIED BY tv9;
CREATE user C##TV10 IDENTIFIED BY tv10;

GRANT CREATE SESSION TO C##TV1;
GRANT CREATE SESSION TO C##TV2;
GRANT CREATE SESSION TO C##TV3;
GRANT CREATE SESSION TO C##TV4;
GRANT CREATE SESSION TO C##TV5;
GRANT CREATE SESSION TO C##TV6;
GRANT CREATE SESSION TO C##TV7;
GRANT CREATE SESSION TO C##TV8;
GRANT CREATE SESSION TO C##TV9;
GRANT CREATE SESSION TO C##TV10;


-- role cho mọi user
create role C##ThanhVien;
grant connect, resource to C##ThanhVien;
grant select on C##QLBB.THANHVIEN to C##ThanhVien;
grant execute on DECRYPT_TV_LUONG to C##ThanhVien;
grant create session to C##ThanhVien;
GRANT SELECT ON TAIKHOAN_QUYEN TO C##ThanhVien;


--role cho ban tổ chức
create role C##BTC_BB;

grant connect,resource to C##BTC_BB;
grant C##ThanhVien to C##BTC_BB;
GRANT SELECT ON TAIKHOAN_QUYEN TO C##BTC_BB;

--BTC được xem, xóa, sửa trong các table dưới
grant select, delete, update on UNGCUVIEN to C##BTC_BB;
grant select, delete, update on TO_LAP to C##BTC_BB;
grant select, delete, update on THEODOI to C##BTC_BB;
grant select, delete, update on GIAMSAT to C##BTC_BB;

--role cho thành viên tổ lâp
create role C##ToLap_BB;

grant connect, resource to C##ToLap_BB;
grant C##ThanhVien to C##ToLap_BB;
GRANT SELECT ON TAIKHOAN_QUYEN TO C##ToLap_BB;

--do không thể gán quyền chi tiết tới cột cho role, ta dùng view
--view này gồm các cột trong bảng THANHVIEN ngoại trừ cột DONVI, CHINHANH, CONGTAC, TAMNGHI, LUONG là các thông tin nhạy cảm
create view ToLap as
select MA_THANHVIEN, HOTEN, GIOITINH, QUEQUAN, NAMSINH, QUOCTICH, DC_THUONGTRU, DC_TAMTRU FROM C##QLBB.THANHVIEN
WHERE MA_THANHVIEN IN (SELECT MA_THANHVIEN FROM C##QLBB.PHIEUBAU);

--các thành viên trong tổ lập có thể xem được view này
GRANT SELECT ON ToLap TO C##ToLap_BB;

--role cho thành viên tổ theo dõi
create role C##TheoDoi_BB;

grant connect, resource to C##TheoDoi_BB;
grant C##ThanhVien to C##TheoDoi_BB;
GRANT SELECT ON TAIKHOAN_QUYEN TO C##TheoDoi_BB;


--được phép xem thông tin những người đã đi bầu
create view TheoDoi1 as
select MA_THANHVIEN, HOTEN, GIOITINH, QUEQUAN, NAMSINH, QUOCTICH, DC_THUONGTRU, DC_TAMTRU FROM C##QLBB.THANHVIEN
WHERE MA_THANHVIEN IN (SELECT MA_THANHVIEN FROM C##QLBB.PHIEUBAU);

--xem số lần các ứng cử viên được bầu
create view TheoDoi2 as
select UCV1, UCV2, UCV3 FROM C##QLBB.PHIEUBAU;

grant select on TheoDoi1 to C##TheoDoi_BB;
grant select on TheoDoi2 to C##TheoDoi_BB;

--role cho thành viên giám sát
create role C##GiamSat_BB;

--được xem tất cả thông tin nhưng không được thay đổi
grant connect, resource to C##GiamSat_BB;
grant select any table to C##GiamSat_BB;
grant C##ThanhVien to C##GiamSat_BB;
GRANT SELECT ON TAIKHOAN_QUYEN TO C##GiamSat_BB;


--test

--connect as QLBB

select * from C##QLBB.THANHVIEN;
Vnt ThanhVien to TV6;

create user TV1 identified by "tv1";
grant C##BTC_BB to TV1;

create user TV5 identified by "tv5";
grant C##ToLap_BB to TV5;

create user TV4 identified by "tv4";
grant C##TheoDoi_BB to TV4;

create user TV3 identified by "tv3";
grant C##GiamSat_BB to TV3;

var test_tv refcursor;
exec C##QLBB.DECRYPT_TV_LUONG(:test_tv);
print test_tv;