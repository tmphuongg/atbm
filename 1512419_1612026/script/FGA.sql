--Đăng nhập sysdba
connect sys

-- Tạo chính sách
BEGIN
DBMS_FGA.ADD_POLICY
(
object_schema 	=> 'C##QLBB',
object_name   	=> 'TAIKHOAN_QUYEN',
policy_name   	=> 'log_quyen',
audit_column 	=> 'QUYEN',
enable          =>  TRUE,
statement_types => 'INSERT,UPDATE,DELETE,SELECT',
audit_trail => DBMS_FGA.DB+DBMS_FGA.EXTENDED
);
END;
/



-- Đăng nhập QLBB để xem kết quả giám sát
SELECT * FROM DBA_FGA_AUDIT_TRAIL;