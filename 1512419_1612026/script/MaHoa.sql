-- connect as sysdba and run this
--user QLBB is admin
GRANT EXECUTE ON SYS.DBMS_CRYPTO TO QLBB;
grant all PRIVILEGES to QLBB with admin option;

--reconnect as QLBB and run this

--------------------------------------------------------
--  DDL for Package CODING_VC2
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "C##QLBB"."CODING_VC2" AS
    FUNCTION ENCODE (P_DATA IN VARCHAR2) RETURN RAW;
    FUNCTION DECRYPTION (P_DATA IN RAW, POS IN INT) RETURN VARCHAR2;
    END CODING_VC2;

/
--------------------------------------------------------
--  DDL for Package Body CODING_VC2
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "C##QLBB"."CODING_VC2" IS
    P_KEY RAW(256) := UTL_RAW.CAST_TO_RAW('16120261512419');

    --Funtion Encode
    FUNCTION ENCODE (P_DATA IN VARCHAR2) RETURN RAW
    IS
        L_DATA RAW(256) := UTL_RAW.CAST_TO_RAW(P_DATA);
        L_ENCRYPTED RAW(256);
    BEGIN
        NULL;

        L_ENCRYPTED := DBMS_CRYPTO.ENCRYPT
        (
            SRC => L_DATA,
            TYP => DBMS_CRYPTO.DES_CBC_PKCS5,
            KEY => P_KEY
        );
        RETURN L_ENCRYPTED;
    END;

    --function Decryption
    FUNCTION DECRYPTION (P_DATA IN RAW, POS IN INT) RETURN VARCHAR2
    IS
        L_DECRYPTED RAW(256);
        L_TEMP VARCHAR2(32);
    BEGIN
        L_DECRYPTED := DBMS_CRYPTO.DECRYPT
        (
            SRC => P_DATA,
            TYP => DBMS_CRYPTO.DES_CBC_PKCS5,
            KEY => P_KEY
        );
        L_TEMP := UTL_RAW.CAST_TO_VARCHAR2(L_DECRYPTED);
    RETURN SUBSTR(L_TEMP, POS);
    END;
END;

/
--------------------------------------------------------
--  DDL for Synonymn DBMS_CRYPTO
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE PUBLIC SYNONYM "DBMS_CRYPTO" FOR "SYS"."DBMS_CRYPTO";



--sceret information: member's salary
--different member could have same salary -> encrypt salary with member_id to have unique encrypt
UPDATE THANHVIEN
SET LUONG = CODING_VC2.ENCODE(CONCAT(MA_THANHVIEN, LUONG));

--decrypt with pos -> member_id.length
--only member can see their salary
--------------------------------------------------------
--  DDL for Procedure DECRYPT_TV_LUONG
--------------------------------------------------------
set define off;

  CREATE OR REPLACE EDITIONABLE PROCEDURE "C##QLBB"."DECRYPT_TV_LUONG" 
(
    RC OUT SYS_REFCURSOR
)
AS
    BEGIN
        OPEN RC FOR
            SELECT MA_THANHVIEN, HOTEN, GIOITINH, QUEQUAN, NAMSINH,
                   QUOCTICH, DC_THUONGTRU, DC_TAMTRU, DONVI, CHINHANH,
                   CONGTAC,TAMNGHI, C##QLBB.CODING_VC2.DECRYPTION(LUONG, LENGTH(MA_THANHVIEN)+1) AS LUONG
                FROM C##QLBB.THANHVIEN WHERE USER = MA_THANHVIEN
            UNION
            SELECT * FROM C##QLBB.THANHVIEN WHERE USER != MA_THANHVIEN;
    END DECRYPT_TV_LUONG;

/